#ifndef OPENRGBK55MACROSPLUGIN_H
#define OPENRGBK55MACROSPLUGIN_H

#include "OpenRGBPluginInterface.h"
#include "ResourceManager.h"

#include <QObject>
#include <QString>
#include <QtPlugin>
#include <QWidget>

class OpenRGBK55MacrosPlugin : public QObject, public OpenRGBPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID OpenRGBPluginInterface_IID)
    Q_INTERFACES(OpenRGBPluginInterface)

public:
    ~OpenRGBK55MacrosPlugin() {};

    OpenRGBPluginInfo       PInfo;
    OpenRGBPluginInfo       Initialize(bool, ResourceManager*)   override;
    QWidget*                CreateGUI(QWidget *Parent)           override;
    static bool             DarkTheme;
    static ResourceManager* RMPointer;

};

#endif // OPENRGBK55MACROSPLUGIN_H
