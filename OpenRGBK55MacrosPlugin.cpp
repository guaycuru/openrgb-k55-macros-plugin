#include "OpenRGBK55MacrosPlugin.h"
#include <QHBoxLayout>

bool OpenRGBK55MacrosPlugin::DarkTheme = false;
ResourceManager* OpenRGBK55MacrosPlugin::RMPointer = nullptr;

OpenRGBPluginInfo OpenRGBK55MacrosPlugin::Initialize(bool Dt, ResourceManager *RM)
{
    PInfo.PluginName         = "Corsair K55 Macros plugin";
    PInfo.PluginDescription  = "Plugin to enable simple macro functionality for the Corsair K55 RGB Keyboard";
    PInfo.PluginLocation     = "TopTabBar";
    PInfo.HasCustom          = true;
    PInfo.PluginLabel        = new QLabel("Corsair K55 Macros plugin");

    RMPointer                = RM;
    DarkTheme                = Dt;

    return PInfo;
}

QWidget* OpenRGBK55MacrosPlugin::CreateGUI(QWidget* parent)
{  
    QWidget* widget =  new QWidget(nullptr);
    QHBoxLayout* layout = new QHBoxLayout();
    widget->setLayout(layout);

    layout->addWidget(new QLabel("Allo, allo?"));

    return widget;
}
