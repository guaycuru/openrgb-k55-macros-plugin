# Corsair K55 Macros Plugin

## What is this?

A plugin to allow using macro keys on the Corsair K55 Keyboard when OpenRGB is controlling its leds.
This is needed because OpenRGB puts the keyboard in "software mode" in order to be able to control itds leds.
When in "software mode" the macros stored in the keyboard memory don't work, so we need to run macros in software instead.

